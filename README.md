# DSA2000 Fiber Transmitter

This repository contains the hardware design for the RF over fiber transmitter module.
This module provides the antenna-local analog signal processing post-LNA and drives the laser of the RF over fiber link.
Additionally, this module provides several monitor and control points that will eventually be integrated into the telescope's global monitor and control system.

## Hardware

Designed using KiCad 7, requires our [library](https://gitlab.com/dsa-2000/hardware/kicad-library). This is included as a git submodule, so if you clone this project, ensure you pass `--recurse-submodules`.

```sh
git clone --recurse-submodules https://gitlab.com/dsa-2000/hardware/rfof/fiber-transmitter.git
```

### Artifacts

As part of the CI/CD pipeline for this project, schematics, board files, BoMs, etc. are rendered on each change. Here are the links to the latest version of each:

[Schematic](https://gitlab.com/api/v4/projects/45244891/jobs/artifacts/main/raw/hardware/Fabrication/Schematic.pdf?job=render_schematic)
